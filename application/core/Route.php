<?php

class Route
{
    static function start(){
       $segments = explode('/', $_SERVER['REQUEST_URI']);

       if(!empty($segments)){
           foreach ($segments as $key=> $segment){
               $segments[$key] = strtolower($segment);
           }
       }


       $controllerName = 'Main';
       $actionName = 'index';
       $itemId = NULL;


       if(!empty($segments[1])){
           $controllerName =  ucfirst($segments[1]);

           if(!empty($segments[2])){
               $actionName = $segments[2];

               if(!empty($segments[3])){
                       $itemId = intval($segments[3]);
               }
           }
       }


       $controllerFile = 'Controller_'.$controllerName;
       $actionName = 'action_'.$actionName;

       $controllerPath = 'application/controllers/'.$controllerFile.'.php';

       if(file_exists($controllerPath)){
           require_once $controllerPath;

           $controller = new $controllerFile();

           if(method_exists($controller, $actionName)){

               if(!is_null($itemId)){

                   if(is_numeric($itemId) && $itemId >= 1){

                       $itemId = intval($segments[3]);
                   }else{
                       Route::errorPage404();
                   }
               }
               $controller->$actionName($itemId);
           }else{
               Route::errorPage404();
           }

       }else{
           Route::errorPage404();
       }



    }

    static function errorPage404(){
        echo '<h3>Page not found</h3>';
        exit();
    }
}