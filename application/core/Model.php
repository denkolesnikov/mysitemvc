<?php

class Model
{
    public $db;

    public function __construct()
    {
        try{
            $pdoDB = new PDO('mysql:host=localhost;dbname=test_db','root','');
            $pdoDB->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $pdoDB->exec("SET NAMES 'utf8'");
            $this->db = $pdoDB;
        }catch(PDOException $e){
            exit('failed connection to database<br>'.$e->getMessage());
        }
    }
}