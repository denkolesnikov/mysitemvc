<?php

class Model_Articles extends Model
{
 public  function getArticles(){

         $sql = 'SELECT * FROM articles';
            try{
                $res =  $this->db->query($sql);
                $articles = $res->fetchAll();

            }catch(PDOException $e){
                $articles = 'failed to SELECT from articles<br>'.$e->getMessage();
            }

     return $articles;
 }

    public  function getArticle($ar_id){

        $sql = "SELECT * FROM articles WHERE id = :id";
        try{
            $stmt = $this->db->prepare($sql);
            $stmt->bindValue(':id',$ar_id);
            $stmt->execute();
            $article = $stmt->fetchAll()[0];
        }catch(PDOException $e){
            $article = 'failed to SELECT database entry<br>'.$e->getMessage();
        }

        return $article;
    }

}