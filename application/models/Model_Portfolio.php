<?php
/**
 * Created by PhpStorm.
 * User: Колесников Денис
 * Date: 26.12.2017
 * Time: 0:19
 */

class Model_Portfolio extends Model
{
    public  function getProjects(){

        $sql = 'SELECT * FROM portfolio';
        try{
            $res =  $this->db->query($sql);
            $projects = $res->fetchAll();

        }catch(PDOException $e){
            $projects = 'failed to SELECT from portfolio<br>'.$e->getMessage();
        }

        return $projects;
    }

    public  function getProject($pr_id){

        $sql = "SELECT * FROM portfolio WHERE id = :id";
        try{
            $stmt = $this->db->prepare($sql);
            $stmt->bindValue(':id',$pr_id);
            $stmt->execute();
            $project = $stmt->fetchAll()[0];
        }catch(PDOException $e){
            $project = 'failed to SELECT database entry from portfolio<br>'.$e->getMessage();
        }

        return $project;
    }
    public  function getYears(){

        $sql = 'SELECT DISTINCT year FROM portfolio ORDER BY year';
        try{
            $res =  $this->db->query($sql);
            $years = $res->fetchAll();

        }catch(PDOException $e){
            $years = 'failed to SELECT from portfolio<br>'.$e->getMessage();
        }

        return $years;
    }

}