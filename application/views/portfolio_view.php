

    <header class="grid col-full">
        <hr>
        <p class="fleft">Portfolio</p>
    </header>


    <aside class="grid col-one-quarter mq2-col-full ttt">
        <p class="mbottom">Keep the same size ratio for thumbnails to avoid breaking the grid because of the margin-bottom.</p>
        <menu>
            <a  id="work_all" class="arrow buttonactive">All</a><br>
            <?php foreach ($data['years'] as $year):?>
                <a  id="year_<?=$year['year']?>" class="arrow"><?=$year['year']?></a><br>
            <?php endforeach;?>
        </menu>
    </aside>

    <section class="grid col-three-quarters mq2-col-full">

        <div class="grid-wrap works">

            <?php foreach ($data['projects'] as $project):?>
                <figure class="grid col-one-third mq1-col-one-half mq2-col-one-third mq3-col-full year_<?=$project['year']?>">
                    <a href="/portfolio/show/<?=$project['id']?>" >
                        <img src="<?=$project['url']?>" alt="" >
                        <span class="zoom"></span>
                    </a>
                    <figcaption>
                        <a href="/portfolio/show/<?=$project['id']?>" class="arrow"><?=$project['title']?></a>
                        <p><?=substr($project['description'], 0, 30);?></p>
                    </figcaption>
                </figure>
            <?php endforeach;?>

        </div> <!-- grid inside 3/4-->

    </section>