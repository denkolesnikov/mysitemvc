
    <section class="grid-wrap" >
        <header class="grid col-full">
            <hr>
            <p class="fleft">Home</p>
            <a href="/about" class="arrow fright">see more infos</a>
        </header>

        <div class="grid col-one-half mq2-col-full">
            <h1>Web design <br>
                Web Development <br>
                Graphic Design</h1>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit.
            </p>
            <p>Vivamus pharetra posuere sapien. Nam consectetuer. Sed aliquam, nunc eget euismod ullamcorper, lectus nunc ullamcorper orci, fermentum bibendum enim nibh eget ipsum.
            </p>
        </div>


        <div class="slider grid col-one-half mq2-col-full">
            <div class="flexslider">
                <div class="slides">
                    <div class="slide">
                        <figure>
                            <img src="/assets/img/portfolio/21060.jpg" alt="">
                            <figcaption>
                                <div>
                                    <h5>Caption 1</h5>
                                    <p>Lorem ipsum dolor set amet, lorem <a href="#">link text</a></p>
                                </div>
                            </figcaption>
                        </figure>
                    </div>

                    <div class="slide">
                        <figure>
                            <img src="/assets/img/portfolio/wallpaper-abstract-iphone-wallpaper.jpg" alt="">
                            <figcaption>
                                <div>
                                    <h5>Caption 2</h5>
                                    <p>Fusce dapibus, tellus ac cursus commodo <a href="#">link text</a></p>
                                </div>
                            </figcaption>
                        </figure>
                    </div>

                    <div class="slide">
                        <figure>
                            <img src="/assets/img/portfolio/architecture-abstract-6.jpg" alt="">
                            <figcaption>
                                <div>
                                    <h5>Caption 3</h5>
                                    <p>Fusce dapibus, tellus ac cursus commodo <a href="#">link text</a></p>
                                </div>
                            </figcaption>
                        </figure>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <section class="services grid-wrap">
        <header class="grid col-full">
            <hr>
            <p class="fleft">Services</p>
            <a href="#" class="arrow fright">see more services</a>
        </header>

        <article class="grid col-one-third mq3-col-full">
            <aside>01</aside>
            <h5>Web design</h5>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim.</p>
        </article>

        <article class="grid col-one-third mq3-col-full">
            <aside>02</aside>
            <h5>Web development</h5>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim.</p>
        </article>

        <article class="grid col-one-third mq3-col-full">
            <aside>03</aside>
            <h5>Graphic design</h5>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim.</p>
        </article>
    </section>

    <section class="works grid-wrap">
        <header class="grid col-full">
            <hr>
            <p class="fleft">Portfolio</p>
            <a href="/portfolio" class="arrow fright">see more projects</a>
        </header>

        <?php foreach ($data['portfolio'] as $project):?>
            <figure class="grid col-one-third mq1-col-one-half mq2-col-one-third mq3-col-full year_<?=$project['year']?>">
                <a href="/portfolio/show/<?=$project['id']?>" >
                    <img src="<?=$project['url']?>" alt="" >
                    <span class="zoom"></span>
                </a>
                <figcaption>
                    <a href="/portfolio/show/<?=$project['id']?>" class="arrow"><?=$project['title']?></a>
                    <p><?=substr($project['description'], 0, 30);?></p>
                </figcaption>
            </figure>
        <?php endforeach;?>
    </section>