
    <header class="grid col-full">
        <hr>
        <p class="fleft">Project</p>
    </header>


    <aside class="grid col-one-quarter mq2-col-one-third mq3-col-full">

        <p class="mbottom"><?=$data['project']['description']?></p>
        <h6>Client</h6>
        <p class="halfmbottom">Company Y, a design studio in Auckland</p>

        <h6>Details</h6>
        <ul class="halfmbottom">
            <li>Design (Logo, UI, ...)</li>
            <li>Front-end development</li>
        </ul>

        <h6>Technology</h6>
        <ul class="halfmbottom">
            <li>HTML5</li>
            <li>CSS3</li>
            <li>jQuery</li>
        </ul>

        <h6>Links</h6>
        <ul class="mbottom">
            <li><a href="http://">Live website</a> - http://www.??.com</li>
            <li><a href="http://">Original size</a> - 6.4 Mo</li>
        </ul>

        <h6>Testimonial</h6>
        <blockquote>Thanks for that amazing work, more than happy with the result</blockquote>

    </aside>

    <section class="grid col-three-quarters mq2-col-two-thirds mq3-col-full">

        <figure class="">
            <a href="#" >
                <img src="<?=$data['project']['url']?>" alt="" >
            </a>
            <figcaption>
                <p><h3><?=$data['project']['title']?></h3></p>
                <p><?=$data['project']['year']?></p>
            </figcaption>
        </figure>

    </section>
