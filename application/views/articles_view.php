
    <header class="grid col-full">
        <hr>
        <p class="fleft">Articles</p>
    </header>


    <section class="grid col-three-quarters mq2-col-two-thirds mq3-col-full">

        <?php foreach ($data['articles'] as $article):?>
            <article class="post">
                <h4><a href="/articles/show/<?=$article['id']?>" class="post-title"><b><?=$article['title']?></b></a></h4>
                <div class="meta">
                    <p>Posted on <span class="time">November 15, 2011</span> by <a href="#" class="fn"><?=$article['author']?></a> in <a href="#"class="cat">Other</a> with <a href="#" class="comments-link">42 comments</a>.</p>
                </div>
                <div class="entry">
                    <p><?=substr($article['text'], 0, 400);?></p>
                </div>
                <footer>
                    <a href="/articles/show/<?=$article['id']?>" class="more-link">Continue reading…</a>
                </footer>
            </article>
        <?php endforeach;?>

        <ul class="page-numbers">
            <li><a href="#">Prev</a></li>
            <li><a href="#" class="current">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">Next</a></li>
        </ul>

    </section>

    <aside class="grid col-one-quarter mq2-col-one-third mq3-col-full blog-sidebar">

        <div class="widget">
            <input id="search" type="search" name="search" placeholder="Type and hit enter to search" >
        </div>

        <div class="widget">
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim.</p>
        </div>

        <div class="widget">
            <h2>Popular Posts</h2>
            <ul>
                <li><a href="#" title="">Nullam porttitor elementum ligula</a></li>
                <li><a href="#" title="">Vestibulum interdum</a></li>
                <li><a href="#" title="">Quisque venenatis ante sit amet dolor</a></li>
                <li><a href="#" title="">Aliquam adipiscing libero vitae leo</a></li>
                <li><a href="#" title="">Sed accumsan quam ac tellus</a></li>
            </ul>
        </div>

        <div class="widget">
            <h2>Categories</h2>
            <ul>
                <li><a href="http://">Design (99+)</a></li>
                <li><a href="http://">Web (53)</a></li>
                <li><a href="http://">Other (12)</a></li>
                <li><a href="http://">Weird (4)</a></li>
            </ul>
        </div>

        <div class="widget">
            <h2>Meta</h2>
            <ul>
                <li><a href="">Entries (RSS)</a></li>
                <li><a href="">Comments (RSS)</a></li>
            </ul>
        </div>
    </aside>