<?php
/**
 * Created by PhpStorm.
 * User: Колесников Денис
 * Date: 26.12.2017
 * Time: 23:15
 */

class Controller_About extends Controller
{

    public  function __construct()
    {
        parent::__construct();
    }

    public function action_index(){
        $this->view->generate('about');
    }
}