<?php
/**
 * Created by PhpStorm.
 * User: Колесников Денис
 * Date: 26.12.2017
 * Time: 0:18
 */

class Controller_Portfolio extends Controller
{
    public $model;

    public  function __construct()
    {
        parent::__construct();
        require 'application/models/Model_Portfolio.php';
        $this->model = new Model_Portfolio();
    }

    public function action_index(){
        $data['projects'] = $this->model->getProjects();
        $data['years'] = $this->model->getYears();
        $this->view->generate('portfolio' ,$data);
    }

    public function action_show($id){
        $data['project'] = $this->model->getProject($id);
        $this->view->generate('project' ,$data);
    }


}