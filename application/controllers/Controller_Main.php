<?php

class Controller_Main extends Controller
{
    public $model;

    public  function __construct()
    {
        parent::__construct();
        require 'application/models/Model_Main.php';
        $this->model = new Model_Main();
    }

    public function action_index(){
        $data['portfolio'] = $this->model->getPortfolio();
        $this->view->generate('main',  $data);
    }

}