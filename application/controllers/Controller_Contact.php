<?php
/**
 * Created by PhpStorm.
 * User: Колесников Денис
 * Date: 26.12.2017
 * Time: 23:24
 */

class Controller_Contact extends Controller
{
    public  function __construct()
    {
        parent::__construct();
    }

    public function action_index(){
        $this->view->generate('contact');
    }
}