<?php

class Controller_Articles extends Controller
{
    public $model;

    public  function __construct()
    {
        parent::__construct();
        require 'application/models/Model_Articles.php';
        $this->model = new Model_Articles();
    }

    public function action_index(){
        $data['articles'] = $this->model->getArticles();
        $this->view->generate('articles' ,$data);
    }

    public function action_show($id){
        $data['article'] = $this->model->getArticle($id);
        $this->view->generate('article' ,$data);
    }
}