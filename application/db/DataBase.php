<?php
/**
 * Created by PhpStorm.
 * User: Колесников Денис
 * Date: 30.12.2017
 * Time: 20:51
 */

class DataBase
{
    static function migrate(pdo $pdoDB){
        try{
            $sqlQuery = [

                'CREATE TABLE articles(
                   id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
		           title VARCHAR(80),
		           text  TEXT,
		           author VARCHAR(70)
		         )DEFAULT CHARACTER SET utf8 ENGINE=InnoDB 
	           ',
                'CREATE TABLE portfolio(
		          id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                  url VARCHAR(100),
                  title VARCHAR(75),
                  description  TEXT,
                  year YEAR
	         	)DEFAULT CHARACTER SET utf8 ENGINE=InnoDB 
	'];

            foreach ($sqlQuery as $query){
                $result [] = $pdoDB->exec($query);
            }


        }catch(PDOException $e){
            $result ='failed to creat table or DataBase<br>'.$e->getMessage();
        }

        return $result;
    }

    static function seed(pdo $pdoDB , $requests){
        try{
            foreach ($requests as $request){

                foreach ($request as $sql){

                    $result [] = $pdoDB->exec($sql);
                }
            }

        }catch(PDOException $e){
            $result = 'cannot insert to table<br>'.$e->getMessage();
        }

        return $result;
    }
}