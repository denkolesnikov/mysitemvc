<?php
require_once 'application/core/Model.php';
require_once 'DataBase.php';

$table1 = 'articles';
$table2 = 'portfolio';


    $requests = [

        //--------------Articles---------------
        [
            'INSERT INTO '.$table1.' SET
	             title = "Voyager 1 just fired up some thrusters",
	             text = "When Voyager 1\'s trajectory correction maneuver thrusters last fired, Ronald Reagan had just been elected president. Over 30 years ago, about a decade into the spacecraft\'s journey out to the edge of our solar system and beyond, the thrusters had officially served their purpose. The trajectory correction maneuver (TCM) thrusters sent out little puffs of power to correct the object\'s course, allowing Voyager 1 to explore Jupiter, Saturn, and several moons orbiting them. After the last course correction for Saturn on November 8, 1980, the TCMs went silent.",
	             author = "Rachel Feltman";
	',
            'INSERT INTO '.$table1.' SET
	             title = "Pluto is way cooler than it should be",
	             text = "It was a lazy, hazy, crazy day on Pluto when a spacecraft from Earth flew by at a blistering speed of 31,000 miles per hour two years ago.New Horizons took a bunch of snapshots, made some quick measurements of Pluto’s atmosphere, and sent them all back here, giving planetary scientists their first up-close look of the distant dwarf planet.",
	             author = "Mary Beth Griggs";
	',
            'INSERT INTO '.$table1.' SET
	             title = "One weird thing about eclipses",
	             text = "This summer, for the first time since 1918, a total solar eclipse will cut a path across the mainland United States. On August 21, everyone in North America will be able to watch the moon pass in front of the sun, blotting out some or all of its light (depending on where you live). People near Lincoln City, Oregon will see the total eclipse around 9:05am PDT. Then the path of totality slants eastward, finishing up in South Carolina at 2:43pm EDT.",
	             author = "Sarah Fecht";
	',
            'INSERT INTO '.$table1.' SET
	             title = "One in five ATMs now charge customers to withdraw cash",
	             text = "One in five ATMs now charge customers to withdraw cash, consumer watchdog Which? says and is calling for an urgent review.It comes as thousands of machines are being shutdown.Which? said it has \"significant concerns\" has written to the Payment Systems Regulator (PSR),calling for it to conduct an urgent market review on the potential impact on consumers.",
	             author = "BLOOMBERG NEWS";
	',
            'INSERT INTO '.$table1.' SET
	             title = "Snow and ice set to cause chaos for commuters",
	             text = "Almost a foot of powder is expected to be dumped across the country as temperatures plummet to -12 in the biggest whiteout since 2013. Snow and ice may reach as far as Devon and Cornwall and stretch up to the far north of Scotland and temperatures will struggle to rise above freezing, forecasters say.Approximately four inches of snow will fall within a three hour spell, known as a snow bomb, causing the most significant amount of chaos for years",
	             author = "Agence France-Presse";
	',
            'INSERT INTO '.$table1.' SET
	             title = "Facebook criticised over failure to stop counterfeit designer goods being sold",
	             text = "It comes as counterfeit traders have been openly selling fake luxury brands on the social media\'s Marketplace site.An undercover investigation by BBC South East discovered that even after they had received fake goods ordered off the forum and reported it to Facebook the sellers were still continuing to operate.Mike Andrews, lead co-ordinator of the National Trading Standards eCrime Team, says Facebook needs to do more to tackle the problem.",
	             author = "Reuters News Agency";
	'
        ],
    //--------------Portfolio-------/assets/img/portfolio/---------------
    [
        'INSERT INTO '.$table2.' SET
                 url = "/assets/img/portfolio/",
	             title = "Snow and ice set",
	             description = "It comes as counterfeit traders have been openly selling fake luxury brands on the social media\'s Marketplace site.",
	             year = "2014";
	',
        'INSERT INTO '.$table2.' SET
                 url = "/assets/img/portfolio/WHDQ-512567808.png",
	             title = "One in five ATMs",
	             description = "A amet atque dolor, expedita impedit ipsam numquam. Animi at blanditiis corporis deleniti dolor doloribus duci",
	             year = "2015";
	',
        'INSERT INTO '.$table2.' SET
                 url = "/assets/img/portfolio/wallpaper-abstract-iphone-wallpaper.jpg",
	             title = "Echoes of the unknown",
	             description = "Atque consectetur distinctio dolore ea ex ipsa iste, minus nemo rerum voluptate! Animi consequatur fuga officiis quaerat qui, soluta veniam. ",
	             year = "2016";
	',
        'INSERT INTO '.$table2.' SET
                 url = "/assets/img/portfolio/Square_Abstrac.jpg",
	             title = "Silent Melody",
	             description = "Aut, eaque enim itaque magnam nulla quibusdam ratione veritatis? Ad adipisci aliquid amet animi asperiores beatae cum debitis distinctio dol",
	             year = "2015";
	',
        'INSERT INTO '.$table2.' SET
                 url = "/assets/img/portfolio/IMG_1074565.jpg",
	             title = "Rush Hour",
	             description = "Ea ex labore maxime quaerat suscipit unde? Asperiores consequatur est incidunt ipsa nihil, obcaecati odit vero voluptate. Autem earum nihil",
	             year = "2014";
	',
        'INSERT INTO '.$table2.' SET
                 url = "/assets/img/portfolio/Bulb-New-Photos.jpg",
	             title = "Life is beautiful",
	             description = "Laudantium nemo numquam obcaecati odio placeat quaerat quas rem repellat tempore temporibus ullam unde veniam.",
	             year = "2017";
	',
        'INSERT INTO '.$table2.' SET
                 url = "/assets/img/portfolio/architecture-abstract-6.jpg",
	             title = "A Mermaid",
	             description = "Corporis inventore libero nam nihil quisquam vel voluptate! Animi autem commodi delectus esse, ipsa molestiae provident quaerat quia quide",
	             year = "2016";
	',
        'INSERT INTO '.$table2.' SET
                 url = "/assets/img/portfolio/Abstract-ages-f.jpg",
	             title = "Discovery",
	             description = "Cumque debitis doloribus eaque earum illum impedit nemo nulla officiis recusandae unde, veniam voluptatibus?",
	             year = "2017";
	',
        'INSERT INTO '.$table2.' SET
                 url = "/assets/img/portfolio/abstract-moon-fantasy-art-islands.jpg",
	             title = "All Around Me",
	             description = "Ducimus harum nihil numquam possimus quis vero voluptate! Ad consectetur delectus dolores dolorum, eius explicabo facere itaque laud",
	             year = "2014";
	',
        'INSERT INTO '.$table2.' SET
                 url = "/assets/img/portfolio/21060.jpg",
	             title = "In Rainbows",
	             description = "Minima nesciunt perferendis provident sapiente sequi tenetur ut vel? Accusantium amet animi architecto aspernatur assumenda commodi conseq",
	             year = "2015";
	',
        'INSERT INTO '.$table2.' SET
                 url = "/assets/img/portfolio/1e062f5014.jpg",
	             title = "Dragon Tree",
	             description = "Uuntur distinctio eaque est facilis laborum magnam, magni nisi officia quae quibusdam quis quod repellendus reprehenderit sed sint sunt ul",
	             year = "2014";
	',
    ]

    ];

$connectDB = new Model();
DataBase::migrate($connectDB->db);
DataBase::seed($connectDB->db,$requests);